import subprocess
import getpass
from random import random
import sys

if len(sys.argv) == 1:
	print("Bitte gib mir das Programm")
	exit()
if len(sys.argv) > 2:
	print("Bitte gib mir NUR das Programm")
	exit()

program = sys.argv[1]
for n in range(2**8):
	# TODO: output bearbeiten!
	output = subprocess.check_output([program, str(n)])
	output = output.decode("utf-8").replace("\n", "")
	if output != bin(255-n)[2:].zfill(8):
		print("Dein Programm funktioniert nicht für {}".format(n))
		exit()
		

print("Sehr gut! Du weißt wie man ein Byte invertiert!")
print("Ist dir aufgefallen, dass man auch die Bytedarstellung von 255 - input verwenden kann?")		
file = open("/home/{}/Orden/Handwerkslehrling".format(getpass.getuser()), "w")
file.write("some ASCII stuff\n")
file.close()
