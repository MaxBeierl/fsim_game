from flask import Flask, render_template, request
app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True


@app.route('/')
def Eingangshalle():
    return render_template("hello.html")
	
@app.route('/aeGha7ahPu')
def level1():
	return render_template("fette_dame.html")
	
@app.route('/level2')
def level2():
	password = request.args.get('password')
	if password == "aith9Neefe":
		return render_template("wasserspeier.html")
	else:
		return render_template("falsch.html")
		
@app.route('/level3')
def level3():
	password = request.args.get('password')
	if password == "IChu3looda":
		return "richtig!"
	else:
		return render_template("falsch.html")