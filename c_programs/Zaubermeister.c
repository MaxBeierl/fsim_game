// evtl im sudoers file für gpasswd das Passwort ausschalten (sudo visudo, GIYF)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>




int main(int argc, char* argv[]){
	if(argc == 1){
		printf("Hallo junger Novize\n");
		printf("Bevor wir deine Ausbildung beginnen können musst du mir helfen\n");
		printf("Ich habe den Schlüssel zur Unterrichtskammer am Fluss verloren\n");
		printf("Der Fluss befindet sich direkt neben unserer Schule\n");
		printf("Bitte gib ihn an, wenn du das nächste mal mit mir sprichst\n");
	} else if (argc > 2){
		printf("babbel mich net müd\n");
		printf("ich möchte nur einen Schlüssel von dir haben\n");
	} else if (strcmp(argv[1], "ThaeJahl0s") == 0){
		printf("Sehr gut, das ist mein Schlüssel!\n");
		printf("Du kannst den Unterrichtsraum nun betreten\n");
		printf("Dort findest du weitere Aufgaben\n");
		setuid(0);
		system("sudo gpasswd -a $(echo ~ | cut -d \"/\" -f 3) novices");
		printf("Eventuell musst du dich erneut einloggen damit du ihn betreten kannst\n");
	} else {
		printf("Das ist leider nicht der richtige Schlüssel\n");
		printf("Denke daran dich genau umzuschauen\n");
		printf("Das geht indem du den \"ls -a\" Befehl verwendest\n");
	}
	return 0;
}
