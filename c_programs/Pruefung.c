#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]){
	
	if(argc != 2){
		printf("Um die Pruefung zu bestehen gib bitte den Pfad zu deinem Programm an - nicht weniger, nicht mehr\n");
		return 0;
	}
	
	FILE *file;
	
	for(int i = 0; i < 10; i++){
		
		int n = rand() % 100; // random test number
		if(i == 9) n = 0; // mean input ;)
		char command[100];
		char return_char;
		int user_result = 0;
		int result = n * (n+1) * (2 * n + 1) / 6; // correct result
		sprintf(command, "%s %s %d", "timeout 30", argv[1], n); // concat strings to correct command
		
		// execute command and test for return value
		if(0 == (file = (FILE*)popen(command, "r"))){
			printf("Dein Programm gibt den Wert nicht korrekt zurück");
			return 0;
		}
		
		// get user program output
		while (fread(&return_char, sizeof return_char, 1, file))
		{
			int a = (int)return_char - 48;
			if(a < 0 || a > 9) continue;
			user_result *= 10;
			user_result += a;
		}
		
		// test user output
		if(user_result != result){
			printf("Dein Programm verrechnet sich bei n = %d\n", n);
			return 0;
		}
	}
	pclose(file);
	
	char command[200];
	sprintf(command, "cp -f /home/pi/fsim_game/templates/pruefung_zaubererschule /home/$(echo \"%s\" | cut -d \'/\' -f 3)/Orden/Pruefung_Zaubererschule", argv[1]);
	
	printf("Sehr gut, du hast die Pruefung bestanden!\n");
	printf("Du erhaeltst eine Belohnung in deiner Ordenssammlung\n");
	printf("Diese findest du in deinem Home-Verzeichnis\n");
	printf("Du kannst auch die der anderen Spieler betrachten\n");
	system(command);
}