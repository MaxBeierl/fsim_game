# FSIM_game

Öffentliches Repository für das Spiel der FSIM der OTH Regensburg.
Mitarbeit ist gerne willkommen! (bleibt aber möglichst lange bei den "best practices for git" ;) )


Lies bitte auch die im doc-Verzeichnis liegenden Dateien, insbesondere die best_practice.txt
Im doc/how_to.txt kannst du sehen wie sich Admin-Aufgaben durchführen lassen.
Wenn du dich beteiligen willst schnapp dir doch erstmal eins der ToDos :)

Setup:
- Raspbian Lite image auf SD-Karte
- Datei "ssh" im Image erstellen um sofort ssh Zugang zu freizuschalten (evtl. google)
- Anschluss über Kabel herstellen (IP im Routermenü o.ä. herausfinden)
- per ssh einloggen ($ ssh pi@[IP]/Windows PUTTY o.ä. verwenden), default PW: raspberry
- evtl. mit "sudo raspi-config" WLAN-Zugang einrichten
- Passwort ändern mit "passwd" Kommando
- $ git clone https://gitlab.com/MaxBeierl/fsim_game.git (Jetzt befindet sich das Repo im "fsim_game" Ordner)
- $ cd scripts
- $ sudo chmod u+x update
- $ ./update
- im Routermenü einen Port auf den ssh-Port des pi's umleiten (default 22)
- sollte ein Webserver laufen auch für diesen Port einrichten (Stichwort "port forwarding")


Kontakt könnt ihr über fsim-ev.de zu uns aufnehmen.
Lizenz wird bald hinzugefügt aber geht im Zweifelsfall davon aus, dass ihr es mit Nennung der Quelle verwenden dürft wie ihr wollt ;)

